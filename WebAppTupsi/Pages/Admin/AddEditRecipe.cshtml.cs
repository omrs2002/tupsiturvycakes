﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TopsyTurvyCakes.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace TopsyTurvyCakes.Pages.Admin
{
    [Authorize]
    public class AddEditRecipeModel : PageModel
    {
        private readonly IRecipesService recipesService;

        [FromRoute]
        public long? Id { get; set; }

        public bool IsNewRecipe
        {
            get { return Id == null; }
        }

        [BindProperty]
        public Recipe Recipe { get; set; }
        [BindProperty]
        public IFormFile Image { get; set; }

        public AddEditRecipeModel(IRecipesService recipesService)
        {
            this.recipesService = recipesService;
        }

        public async Task OnGetAsync()
        {
            Recipe = await recipesService.FindAsync(Id.GetValueOrDefault())
                ?? new Recipe();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            try
            {
                if(!ModelState.IsValid)
                    return Page();

                Recipe.Id = Id.GetValueOrDefault();
                var _recipe = await recipesService.FindAsync(Id.GetValueOrDefault());
                _recipe.Ingredients = Recipe.Ingredients;
                _recipe.Name = Recipe.Name;
                _recipe.Description = Recipe.Description;
                _recipe.Directions = Recipe.Directions;

                if (Image != null)
                    _recipe.SetImage(Image);
                else
                    _recipe.Image = Recipe.Image;

                await recipesService.SaveAsync(_recipe);

                return RedirectToPage("/Recipe", new { id = Id });
            }
            catch
            {
                return null;
            }
        }

        public async Task<IActionResult> OnPostDelete()
        {
            var DeletedID = Id.GetValueOrDefault();
            await recipesService.DeleteAsync(DeletedID);
            return RedirectToPage("/Index");
        }



    }
}